;;; must have
(global-linum-mode 1)

;;; best theme
(load-theme 'tango-dark)

;;; move backup files to emacs folder
(setq backup-directory-alist
      `(("." . ,(concat user-emacs-directory "backups"))))

;;; This disable creation of interlocking files aka #files#, use at your own risk
;;; still creates anoyying file when exiting without saving :(
;(setq create-lockfiles nil)
