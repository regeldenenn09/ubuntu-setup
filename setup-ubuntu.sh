#!/bin/bash
#
#Kopo's ubuntu machine setup on Ubuntu
#
#
sudo apt-get update
sudo apt-get install -y build-essential libssl-dev
sudo apt-get install -y emacs
sudo apt-get install -y python-dev python-pip
sudo apt-get install -y nodejs
sudo apt-get install -y npm
sudo apt-get install -y curl
sudo apt-get install -y tmux
sudo apt-get install -y subversion
node -v
sudo npm install -g nodemon 
sudo apt-get install -y mongodb
sudo systemctl status mongodb
sudo apt-get install -y zsh
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
chsh -s /usr/bin/zsh
cp .zshrc .emacs /home/$(whoami)
source ~/.zshrc

echo "Done"
echo "You might want to install manually:"
echo "discord"
echo "visual code"
echo "postman\n"
echo "Must have on Ubuntu 18.04 / 20.04\n\t WORKSPACE MATRIX !!\n"
